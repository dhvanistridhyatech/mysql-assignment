/*1. Create database using query*/
CREATE DATABASE MyDemo;

/*2.Create table using query (create 2 table members and moviews)*/
/*Member Table:*/
CREATE TABLE members (  
membership_number INT  NOT NULL,  
full_names VARCHAR (20) NOT NULL, 
gender VARCHAR (20) NOT NULL,
date_of_birth DATE  NOT NULL,  
physical_address CHAR (25),
postal_address CHAR (25),
contact_number INT (25),
email VARCHAR(20),
PRIMARY KEY (membership_number));
 -- Movie Table:    
CREATE TABLE movies (  
movie_id INT  NOT NULL,  
title VARCHAR (20) NOT NULL, 
director VARCHAR (20) NOT NULL,
year_released DATE  NOT NULL,  
category_id CHAR (25),
PRIMARY KEY (movie_id)
);  

/*3.Create different-different Select query with DISTINCT,*,FROM, WHERE,GROUP BY,HAVING and HAVING*/
SELECT DISTINCT gender FROM members;
SELECT  `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM members;
SELECT `gender`,`membership_number` FROM members;
SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM members  WHERE gender='female';
SELECT COUNT(membership_number), full_names FROM members GROUP BY gender;
SELECT COUNT(membership_number), gender FROM members GROUP BY gender HAVING COUNT(membership_number) > 2;

/*4.Create query using WHERE Clause(IN, OR, AND, Not IN,Equal To,Not Equal To, Greater than, less than )*/
SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members` WHERE `membership_number` NOT IN (1,2,3);
SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members` WHERE `membership_number` IN (1,2,3);
SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members` WHERE `membership_number` = 1 OR `full_names` = 'janes_notes';
SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members` WHERE `membership_number` ='1' AND `gender` = 'female';
SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM members WHERE  membership_number > 3 ;
SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM members WHERE  membership_number < 4 ;
SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email`  FROM members WHERE membership_number <> 3;
SELECT COUNT(membership_number), gender FROM members GROUP BY gender HAVING COUNT(membership_number) > 2;  /*greater than using diffrent type query*/
SELECT COUNT(membership_number), gender FROM members GROUP BY gender HAVING COUNT(membership_number) <> 2;  /*not equal  using diffrent type query*/
SELECT COUNT(membership_number), gender FROM members GROUP BY gender HAVING COUNT(membership_number) < 4;  /*less than using diffrent type query*/
SELECT COUNT(membership_number), gender FROM members HAVING gender="female";  /*equal  using diffrent type query*/

/*5.Create insert query in Member Table... */
INSERT INTO `members` (`membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email`) VALUES
(1, 'janes_notes', 'female', '1980-07-21', 'First Street Plot No 4', 'Private Bag', 759253542, 'janetjones@yahoo.com'),
(2, 'janet smith jones', 'female', '1980-06-23', 'Melrose 123', 'NULL', 1, 'jj@fstreet.com'),
(3, 'Robert Fill', 'Male', '1980-07-12', '3rd street 34', 'NULL', 12345, 'rm@street.com'),
(4, 'Gloaria Williams', 'female', '1984-02-14', '2nd street 23', 'NULL', 1, 'NULL');

/*Movies Table:*/
INSERT INTO `movies` (`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES
(1, 'Pirates of the Carib', 'Rob Marshell', 2011, '1'),
(2, 'Forgetting Sarah Mar', 'Nocholas  stoller', 2008, '2'),
(3, 'X-MEN', 'NULL', 2008, 'NULL'),
(4, 'CODE-NAME-BLACK', 'Edgar Jimz', 2010, 'NULL'),
(5, 'DADDAY\'S LITTLE GIRL', 'NULL', 2007, '8'),
(6, 'Angels and Demons', 'NULL', 2007, '6'),
(7, 'Divinci Code', 'NULL', 2007, '6'),
(9, 'Honey Monners', 'John Schultz', 2005, '8'),
(16, '67%Guilty', 'NULL', 2012, 'NULL');

/*6.Get record form members table using select query*/
SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members`;

/*7.Getting only the full_names, gender, physical_address and email fields only from memeber table*/
SELECT  `full_names`, `gender`, `physical_address`,`email` FROM `members`;

/*8.Get a member's personal details from members table given the membership number 1*/
SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members` WHERE membership_number='1';

/*9.Get a list of all the movies in category 2 that were released in 2008*/
SELECT `movie_id`,`title`,`year_released`FROM movies WHERE year_released='2008';
SELECT `movie_id`,`title`,`year_released` FROM movies WHERE year_released LIKE '%8'; /*use other query*/

/*10.Gets all the movies in either category 1 or category 2*/
SELECT `movie_id`,`title`,`year_released`,`director`,`category_id` FROM movies WHERE category_id='1' OR category_id='2';

/*11.Gets rows where membership_number is either 1 , 2 or 3*/
SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM members WHERE  membership_number < 4 ;

/*12.Gets all the female members from the members table using the equal to comparison operator.*/
SELECT `membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email` FROM `members` WHERE `gender` = 'female';

/*13.Gets all the payments that are greater than 2,000 from the payments table*/
CREATE TABLE payment(payment_id INT  NOT NULL,  
fname VARCHAR (20) NOT NULL, 
lname VARCHAR (20) NOT NULL,
amount INT  NOT NULL,   
payment_date  DATE  NOT NULL,  
received_payment  DATE  NOT NULL,  
PRIMARY KEY (payment_id));

SELECT `payment_id`,`fname`,`lname`,`amount`,`payment_date`,`received_payment`FROM `payment` WHERE amount>2000;